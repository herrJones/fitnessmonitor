﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;

using FitSDK.Fit;
using FitSDK.Utility;

namespace FitnessMonitor.General {
  /// <summary>
  /// Interaction logic for ucImportFitness.xaml
  /// </summary>
  public partial class ucImportFitness : UserControl {
    Dictionary<ushort, int> mesgCounts = new Dictionary<ushort, int>();
    FileStream fitFile;
    string fitLog;

    public ucImportFitness() {
      InitializeComponent();
    }

    private void btnOpenFile_Click(object sender, RoutedEventArgs e) {
      OpenFileDialog browseDialog = new OpenFileDialog();
      browseDialog.DefaultExt = ".fit";
      browseDialog.Filter = "FIT files (.fit)|*.fit";

      Nullable<bool> result = browseDialog.ShowDialog();

      if (result == true) {
        tbFileName.Text = browseDialog.FileName;
      }
    }

    private void btnProcessFile_Click(object sender, RoutedEventArgs e) {
      fitFile = new FileStream(tbFileName.Text, FileMode.Open);
      mesgCounts = new Dictionary<ushort, int>();

      Decode decodeDemo = new Decode();
      MesgBroadcaster mesgBroadcaster = new MesgBroadcaster();

      // Connect the Broadcaster to our event (message) source (in this case the Decoder)
      decodeDemo.MesgEvent += mesgBroadcaster.OnMesg;
      decodeDemo.MesgDefinitionEvent += mesgBroadcaster.OnMesgDefinition;

      // Subscribe to message events of interest by connecting to the Broadcaster
      mesgBroadcaster.MesgEvent += new MesgEventHandler(OnMesg);
      mesgBroadcaster.MesgDefinitionEvent += new MesgDefinitionEventHandler(OnMesgDefn);

      mesgBroadcaster.FileIdMesgEvent += new MesgEventHandler(OnFileIDMesg);
      mesgBroadcaster.UserProfileMesgEvent += new MesgEventHandler(OnUserProfileMesg);

      fitLog = "";
      Stopwatch fitMeasure = new Stopwatch();
      fitMeasure.Start();

      bool status = decodeDemo.IsFIT(fitFile);
      status &= decodeDemo.CheckIntegrity(fitFile);

      if (status == true) {
        fitLog += "Decoding...\r\n";
        decodeDemo.Read(fitFile);
        fitLog += string.Format("Decoded FIT file {0}\r\n", tbFileName.Text);
      }
      fitMeasure.Stop();
      
      fitLog += string.Format("\r\n\nDecoding took {0} seconds", fitMeasure.Elapsed.Seconds);
      tbFitLog.Text = fitLog;

      fitFile.Close();

      
    }

    #region Message Handlers
    // Client implements their handlers of interest and subscribes to MesgBroadcaster events
    void OnMesgDefn(object sender, MesgDefinitionEventArgs e) {
      fitLog += string.Format("OnMesgDef: Received Defn for local message #{0}, global num {1}\r\n", e.mesgDef.LocalMesgNum, e.mesgDef.GlobalMesgNum);
      tbFitLog.Text += string.Format("\tIt has {0} fields and is {1} bytes long\r\n", e.mesgDef.NumFields, e.mesgDef.GetMesgSize());
    }

    void OnMesg(object sender, MesgEventArgs e) {
      fitLog += string.Format("OnMesg: Received Mesg with global ID#{0}, its name is {1}\r\n", e.mesg.Num, e.mesg.Name);

      for (byte i = 0; i < e.mesg.GetNumFields(); i++) {
        for (int j = 0; j < e.mesg.fields[i].GetNumValues(); j++) {
          switch (e.mesg.fields[i].Num) {
            case Fit.FieldNumTimeStamp:
              if (e.mesg.Num == 20) {
                RecordMesg recordMesg = new RecordMesg(e.mesg);
                fitLog += string.Format("\tField{0} Index{1} (\"{2}\" Field#{4}) Value: {3:d} (raw value {5})\r\n", i, j, e.mesg.fields[i].GetName(), recordMesg.GetTimestamp(), e.mesg.fields[i].Num, e.mesg.fields[i].GetRawValue(j));
              } else
                fitLog += string.Format("\tField{0} Index{1} (\"{2}\" Field#{4}) Value: {3:d} (raw value {5})\r\n", i, j, e.mesg.fields[i].GetName(), e.mesg.fields[i].GetValue(j), e.mesg.fields[i].Num, e.mesg.fields[i].GetRawValue(j));
              break;

            default:
              fitLog += string.Format("\tField{0} Index{1} (\"{2}\" Field#{4}) Value: {3} (raw value {5})\r\n", i, j, e.mesg.fields[i].GetName(), e.mesg.fields[i].GetValue(j), e.mesg.fields[i].Num, e.mesg.fields[i].GetRawValue(j));
              break;
          }
          
        }
      }

      if (mesgCounts.ContainsKey(e.mesg.Num) == true) {
        mesgCounts[e.mesg.Num]++;
      } else {
        mesgCounts.Add(e.mesg.Num, 1);
      }
    }

    void OnFileIDMesg(object sender, MesgEventArgs e) {
      fitLog += string.Format("FileIdHandler: Received {1} Mesg with global ID#{0}", e.mesg.Num, e.mesg.Name);
      FileIdMesg myFileId = (FileIdMesg)e.mesg;
      try {
       // static Manufacturer manuf = new FitSDK.Fit.Manufacturer();

        fitLog += string.Format("\tType: {0}\r\n", myFileId.GetType());
        fitLog += string.Format("\tManufacturer: {0}\r\n", myFileId.GetManufacturer());
        fitLog += string.Format("\tProduct: {0}\r\n", myFileId.GetProduct());
        fitLog += string.Format("\tSerialNumber {0}\r\n", myFileId.GetSerialNumber());
        fitLog += string.Format("\tNumber {0}\r\n", myFileId.GetNumber());
        fitLog += string.Format("\tTimeCreated {0:d}\r\n", myFileId.GetTimeCreated());
        FitSDK.Fit.DateTime dtTime = new FitSDK.Fit.DateTime(myFileId.GetTimeCreated().GetTimeStamp());

      }
      catch (FitException exception) {
        fitLog += string.Format("\tOnFileIDMesg Error {0}\r\n", exception.Message);
        fitLog += string.Format("\t{0}\r\n", exception.InnerException);
      }
    }
    void OnUserProfileMesg(object sender, MesgEventArgs e) {
      fitLog += string.Format("UserProfileHandler: Received {1} Mesg, it has global ID#{0}\r\n", e.mesg.Num, e.mesg.Name);
      UserProfileMesg myUserProfile = (UserProfileMesg)e.mesg;
      try {
        fitLog += string.Format("\tFriendlyName \"{0}\"\r\n", Encoding.UTF8.GetString(myUserProfile.GetFriendlyName()));
        fitLog += string.Format("\tGender {0}\r\n", myUserProfile.GetGender().ToString());
        fitLog += string.Format("\tAge {0}\r\n", myUserProfile.GetAge());
        fitLog += string.Format("\tWeight  {0}\r\n", myUserProfile.GetWeight());
      }
      catch (FitException exception) {
        fitLog += string.Format("\tOnUserProfileMesg Error {0}\r\n", exception.Message);
        fitLog += string.Format("\t{0}\r\n", exception.InnerException);
      }
    }

    void OnDeviceInfoMessage(object sender, MesgEventArgs e) {
      fitLog += string.Format("DeviceInfoHandler: Received {1} Mesg, it has global ID#{0}", e.mesg.Num, e.mesg.Name);
      DeviceInfoMesg myDeviceInfoMessage = (DeviceInfoMesg)e.mesg;
      try {
        fitLog += string.Format("\tTimestamp  {0:d}\r\n", myDeviceInfoMessage.GetTimestamp());
        fitLog += string.Format("\tBattery Status{0}\r\n", myDeviceInfoMessage.GetBatteryStatus());
      }
      catch (FitException exception) {
        fitLog += string.Format("\tOnDeviceInfoMesg Error {0}\r\n", exception.Message);
        fitLog += string.Format("\t{0}\r\n", exception.InnerException);
      }
    }

    void OnMonitoringMessage(object sender, MesgEventArgs e) {
      fitLog += string.Format("MonitoringHandler: Received {1} Mesg, it has global ID#{0}", e.mesg.Num, e.mesg.Name);
      MonitoringMesg myMonitoringMessage = (MonitoringMesg)e.mesg;
      try {
        fitLog += string.Format("\tTimestamp  {0:d}\r\n", myMonitoringMessage.GetTimestamp().ToString());
        fitLog += string.Format("\tActivityType {0}\r\n", myMonitoringMessage.GetActivityType());
        switch (myMonitoringMessage.GetActivityType()) // Cycles is a dynamic field
        {
          case ActivityType.Walking:
          case ActivityType.Running:
            fitLog += string.Format("\tSteps {0}\r\n", myMonitoringMessage.GetSteps());
            break;
          case ActivityType.Cycling:
          case ActivityType.Swimming:
            fitLog += string.Format("\tStrokes {0}\r\n", myMonitoringMessage.GetStrokes());
            break;
          default:
            fitLog += string.Format("\tCycles {0}\r\n", myMonitoringMessage.GetCycles());
            break;
        }
      }
      catch (FitException exception) {
        fitLog += string.Format("\tOnDeviceInfoMesg Error {0}\r\n", exception.Message);
        fitLog += string.Format("\t{0}\r\n", exception.InnerException);
      }
    }
    #endregion
  }
}
